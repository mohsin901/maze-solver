package maze.routing;

// used to throw an exception if no route is found.
public class NoRouteFoundException extends Exception {

    public NoRouteFoundException(String exception){
        super("No Route Found");
    }
    public NoRouteFoundException() {

    }
}
