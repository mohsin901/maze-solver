package maze.routing;

import maze.Maze;
import maze.Tile;

import java.io.*;
import java.util.*;

/**
 * This class contains all the method which will help us solve the maze
 */
public class RouteFinder implements Serializable {
    private Maze maze;
    private Stack<Tile> routeCopy = new Stack<Tile>();
    private List<Tile> routeCopyy = new LinkedList<>();
    private Stack<Tile> route = new Stack<Tile>();
    private boolean finished;

    public RouteFinder(Maze maze) {
        this.maze = maze;
    }

    public Maze getMaze() {
        return maze;
    }

    public List<Tile> getRoute() {
        return route;
    }

    public boolean isFinished() {
        Stack<Tile> routeTemp = route;


        int count=1;
        for(Tile Item: routeTemp){

            if(maze.getAdjacentTile(Item, Maze.Direction.NORTH) == routeTemp.get(count) || maze.getAdjacentTile(Item, Maze.Direction.EAST) == routeTemp.get(count)
             || maze.getAdjacentTile(Item, Maze.Direction.SOUTH) == routeTemp.get(count) || maze.getAdjacentTile(Item, Maze.Direction.WEST) == routeTemp.get(count)){

                 if(count< routeTemp.size()-1){
                     count=count+1;
                 }

                 else if(count == routeTemp.size()-1){
                     return (true);
                 }
            }
            }
        return false;
    }

    /**
     * Here we load a maze
     * @param aString
     * @return
     * @throws IOException
     */
    public static RouteFinder load(String aString) throws IOException {
        try{

            ObjectInputStream is = new ObjectInputStream(new FileInputStream(aString));
            RouteFinder route =(RouteFinder) is.readObject();

            is.close();

        } catch (FileNotFoundException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Here we save a maze
     * @param aString
     */
    public void save (String aString){
        try{
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(aString));
            RouteFinder routeObj = new RouteFinder(getMaze());
            routeObj.route=this.route;
            os.writeObject(routeObj);
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("done");


    }


    /**
     * In this method we keep returning adding the next item of the route copy stack
     * to route. and we return false until the last tile type is an exit.
     * @return
     */
    public boolean step(){

        if (route.size() != 0) {
            if(route.get(route.size()-1).getType() == Tile.Type.EXIT){
                return true;
            }
        }

        if(routeCopy.size()==0){
            solve();
            route.add(routeCopy.get(0));
            routeCopy.remove(0);
            return false;
        }

         else if (routeCopy.size()!= 1){
             route.add(routeCopy.get(0));
             routeCopy.remove(0);
             return false;


        }
        else{
            route.add(routeCopy.get(0));
            return true;
        }
    }

    /**
     * This is the method that solves the maze, if there is a path it will put the route
     * into a stack called routeCopy.
     * @return
     */
    public boolean solve() {
        routeCopy.push(getMaze().getEntrance());
        routeCopy.peek().setTooWall();
        while (true) {
            if (isValid(getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.SOUTH))) {
                if (getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.SOUTH).getType() == Tile.Type.EXIT) {
                    routeCopy.push(getMaze().getExit());
                    break;
                } else if (getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.SOUTH).getType() == Tile.Type.CORRIDOR) {
                    routeCopy.push(getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.SOUTH));
                    routeCopy.peek().setTooWall();
                    continue;
                }
            }


            if (isValid(getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.WEST))) {
                if (getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.WEST).getType() == Tile.Type.EXIT) {
                    routeCopy.push(getMaze().getExit());
                    System.out.println("You Won");
                    break;
                } else if (getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.WEST).getType() == Tile.Type.CORRIDOR) {
                    routeCopy.push(getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.WEST));
                    routeCopy.peek().setTooWall();
                    continue;
                }
            }


            if (isValid(getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.NORTH))) {
                if (getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.NORTH).getType() == Tile.Type.EXIT) {
                    routeCopy.push(getMaze().getExit());
                    break;
                } else if (getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.NORTH).getType() == Tile.Type.CORRIDOR) {
                    routeCopy.push(getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.NORTH));
                    routeCopy.peek().setTooWall();
                    continue;
                }
            }


            if (isValid(getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.EAST))) {
                if (getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.EAST).getType() == Tile.Type.EXIT) {
                    routeCopy.push(getMaze().getExit());
                    break;
                } else if (getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.EAST).getType() == Tile.Type.CORRIDOR) {
                    routeCopy.push(getMaze().getAdjacentTile(routeCopy.peek(), Maze.Direction.EAST));
                    routeCopy.peek().setTooWall();
                    continue;
                }
            }

            routeCopy.pop();
            if (routeCopy.size() <= 0) {
                System.out.println("no path");
                break;
            }

        }
        routeCopy.get(0).setTooEntrance();
        for(int i = 0; i<routeCopy.size();i++){
            if (routeCopy.get(i).getType()== Tile.Type.WALL) {
                routeCopy.get(i).setTooCorridor();

            }}
        return false;



    }

    // checks if tile is valid
    public boolean isValid(Tile aTile){
        return aTile != null;
    }

    //too string method
    @Override
    public String toString() {
        return "RouteFinder{" +
                "maze=" + maze +
                ", route=" + route +
                ", finished=" + finished +
                '}';
    }
}
