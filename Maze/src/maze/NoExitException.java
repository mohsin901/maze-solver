package maze;

/**
 * Here we create a subclass off the invalid maze exception
 * which prints No Exit
 */

public class NoExitException extends InvalidMazeException {
    public NoExitException() {
        super("No Exit");
    }
}