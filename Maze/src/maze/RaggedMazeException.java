package maze;

/**
 * Here we create a subclass off the invalid maze exception
 * which prints ragged maze
 */
public class RaggedMazeException extends InvalidMazeException{
    public RaggedMazeException(){
        super("Ragged Maze");
    }
}
