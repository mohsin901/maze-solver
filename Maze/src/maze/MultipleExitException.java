package maze;

/**
 * Here we create a subclass off the invalid maze exception
 * which prints Multiple Exits
 */

public class MultipleExitException extends InvalidMazeException{
    public MultipleExitException() {
        super("Multiple Exits");
    }
}
