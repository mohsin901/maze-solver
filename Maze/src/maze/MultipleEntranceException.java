package maze;

/**
 * Here we create a subclass off the invalid maze exception
 * which prints Multiple Entrance's
 */

public class MultipleEntranceException extends InvalidMazeException {
    public MultipleEntranceException() {
        super("Multiple Entrance's");
    }
}
