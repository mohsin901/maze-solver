package maze.visualisation;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import maze.InvalidMazeException;
import maze.routing.RouteFinder;
import java.io.IOException;


/**
 * This is the main driver class, it uses javafx to display a window where the user can select
 * what they want to do with the program.
 */
public class MazeApplication extends Application {
    private Object Maze;
    maze.Maze mazeobj = (maze.Maze) Maze;
    RouteFinder route =new RouteFinder(mazeobj);


    @Override
    public void start(Stage stage) throws Exception{
        int screenSize = 600;
        TextField tf1=new TextField();
        Button loadMapButton = new Button("Load Map");
        loadMapButton.setOnAction(value->{
            try {
                mazeobj=maze.Maze.fromTxt(tf1.getText());
                route = new RouteFinder(mazeobj);

            } catch (InvalidMazeException e) {
                e.printStackTrace();
            }
        });

        Button stepButton = new Button("Step");
        Label user_id=new Label("Enter path in the text box and click load map, then click step!");
        user_id.setTextFill(Color.WHITE);
        HBox buttonBox = new HBox(10);
        buttonBox.setAlignment(Pos.CENTER);
        Button loadButton = new Button("Load Route");

        loadButton.setOnAction(value->{
            try {
                RouteFinder.load("maze");

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Button saveButton = new Button("Save Route");

        saveButton.setOnAction(value->{route.save("maze");
        });

        buttonBox.getChildren().addAll(tf1,loadMapButton,loadButton,saveButton);

        stepButton.setOnAction(value->{
            route.step();
            System.out.println(route.getRoute());
            System.out.println(mazeobj.getTileLocation(route.getRoute().get(route.getRoute().size()-1)));
        });

        HBox buttonBox2 = new HBox(10);
        buttonBox2.setAlignment(Pos.BASELINE_CENTER);
        buttonBox2.getChildren().addAll(stepButton,user_id);

        VBox root = new VBox(10);
        root.setBackground(Background.EMPTY);
        root.setAlignment(Pos.CENTER);
        root.getChildren().addAll(buttonBox,buttonBox2);

        Scene scene = new Scene(root, screenSize,screenSize,Color.BLACK);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) throws InvalidMazeException, IOException {
        launch(args);
    }
}
