package maze;


/**
 *Here we have the coordinate class which tells us the
 * location of the tile.
 *
 */
public class Coordinate {
    private int x;
    private int y;

    public Coordinate(int xIn, int yIn) {
        x = xIn;
        y = yIn;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
