package maze;

//Enum of directions
public enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST
}
