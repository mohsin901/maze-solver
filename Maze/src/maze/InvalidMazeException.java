package maze;

/**
 * Here we Have our invalid maze exception which has a constructor
 * which prints a message
 */

public class InvalidMazeException extends Exception {
    public InvalidMazeException(String exception){
        super(exception);
    }
}
