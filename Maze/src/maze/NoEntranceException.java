package maze;

/**
 * Here we create a subclass off the invalid maze exception
 * which prints No Entrance
 */

public class NoEntranceException extends InvalidMazeException {
    public NoEntranceException() {
        super("No entrance");
    }
}
