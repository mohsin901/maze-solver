package maze;

import java.io.Serializable;


/**
 * Here we have the tile class where they can be of 4 different types
 */
public class Tile implements Serializable {
    private Type type;
    private Tile(Type type) {
        this.type = type;
    }
    public  enum Type{
        CORRIDOR,
        ENTRANCE,
        EXIT,
        WALL
    }

    /**
     * Here we pass a character as an argument, and depending on it we will
     * return a tile, otherwise if the character is not valid we return the
     * null.
     * @param aChar
     * @return
     */
    protected static Tile fromChar(char aChar){
       if (aChar == 'e'){
           return new Tile(Type.ENTRANCE);
        }

        else if (aChar == 'x'){
            return new Tile(Type.EXIT);
        }

        else if (aChar == '#')
       {
           return new Tile(Type.WALL);
       }

        else if (aChar == '.'){
           return new Tile(Type.CORRIDOR);
       }

        return null;
    }


    /**
     * Here the toString method converts the tiles into characters so the
     * tiles can be seen from the console.
     * @return
     */
    public String toString() {
       if(getType()==Type.ENTRANCE){
           return "e";
       }
       else if (getType()==Type.EXIT){
           return "x";
       }
       else if (getType()==Type.WALL){
           return "#";
       }
       else if (getType()==Type.CORRIDOR){
           return ".";
       }

       return "\n";
    }


    /**
     * Here we check if the type is not of type wall
     * So we can step through the maze.
     * @return
     */
    public boolean isNavigable(){
        return getType() != Type.WALL;
    }


    // Here we have our getters and setters
    public void setTooWall(){
        setType(Type.WALL);
    }

    public void setTooEntrance(){
        setType(Type.ENTRANCE);
    }

    public void setTooCorridor(){
        setType(Type.CORRIDOR);
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
