package maze;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 *The maze class is essentially where we create a maze object, and create other methods associated with the maze
 * which will help us too solve the correct pathway.
 */

public class Maze implements Serializable {
    private Tile entrance;
    private Tile exit;
    private List<List<Tile>> tiles = new ArrayList<List<Tile>>();
    public static String[] lines;


    /**
     * Here we have a the maze constructor.
     */
    private Maze() {
    }

    public void setTiles(List<List<Tile>> tiles) {
        this.tiles = tiles;
    }

    /**
     *This method loads the maze from the txt file, if it does
     * not match the criteria we throw an exception.
     * @param aMaze
     * @return
     * @throws InvalidMazeException
     */
    public static Maze fromTxt(String aMaze) throws InvalidMazeException {
        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(aMaze)))
        {

            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null)
            {
                contentBuilder.append(sCurrentLine).append("\n");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        String a =contentBuilder.toString();

        int temp_x=0;
        int temp_y=0;

        lines = a.split("\\r?\\n");

        for (String line : lines) {
            if (lines[0].length() != line.length()) {
                throw new RaggedMazeException();
            }
        }

        int entrance = a.length() - a.replace("e", "").length();
        int exit = a.length() - a.replace("x", "").length();


        if (entrance > 1){
            throw new MultipleEntranceException();
        }

        else if (entrance == 0){
            throw new NoEntranceException();
        }

        else if (exit > 1){
            throw new MultipleExitException();
        }

        else if (exit == 0){
            throw new NoExitException();
        }

        Maze mazeObject = new Maze();

        List myList = new ArrayList();


        for(int i = 0; i < a.length(); i++)
        {
            char c = a.charAt(i);

            if(c=='e'){
                mazeObject.setEntrance(Tile.fromChar(c));
                myList.add(mazeObject.getEntrance());
            }

            else if (c=='x'){
                mazeObject.setExit(Tile.fromChar(c));
                myList.add(mazeObject.getExit());
            }
            else {
                myList.add(Tile.fromChar(c));
          }}


        myList.remove(myList.size()-1);
        mazeObject.setTiles(myList);
        mazeObject.tiles.add(myList);
        mazeObject.tiles.remove(mazeObject.tiles.size()-1);

        return mazeObject;


    }

    /**
     * In this method we pass a tile as a paramater and then based on the direction
     * we want we will return that tile. (We just modify the x and y values to get the
     * new tile.)
     * @param aTile
     * @param aDirection
     * @return
     */

    public Tile getAdjacentTile(Tile aTile, Direction aDirection){
        int temp_x = getTileLocation(aTile).getX();
        int temp_y = getTileLocation(aTile).getY();

        if(aDirection==Direction.NORTH){
            temp_y = temp_y +1;
            return getTileAtLocation(new Coordinate(temp_x,temp_y));
        }

        else if(aDirection==Direction.SOUTH){
            temp_y = temp_y -1;
            return getTileAtLocation(new Coordinate(temp_x,temp_y));
        }

        else if(aDirection==Direction.EAST){
            temp_x = temp_x +1;
            return getTileAtLocation(new Coordinate(temp_x,temp_y));
        }

        else if(aDirection==Direction.WEST){
            temp_x = temp_x -1;
            return getTileAtLocation(new Coordinate(temp_x,temp_y));
        }
        return (aTile);
    }

    public Tile getEntrance() {
        return entrance; }

    public Tile getExit() {
        return exit;
    }

    /**
     * Here we have a maze which is a collection of tiles and this method returns
     * the coordinates of the tile that is passed as a paramater.
     * @param aTile
     * @return
     */

    public Coordinate getTileLocation(Tile aTile){
        List myList = new ArrayList();
        myList=tiles;
        int temp_x =lines[0].length()-1;
        int temp_y =0;
        Collections.reverse(myList);

        for(int i = 0; i < myList.size(); i++) {
            if((myList.get(i)) == aTile ){
                try {
                    return new Coordinate(temp_x, temp_y);

                } finally {
                    Collections.reverse(myList);
                }

            }

            else if (tiles.get(i)==null){
                temp_x =lines[0].length()-1;
                temp_y++;}

            else{temp_x=temp_x-1;}
    }
            Collections.reverse(myList);
            return null;
    }

    /**
     * In this method we give a coordinate and return a tile based on the coordinate
     * if there is no tile for the given coordinate we just return null.
     * @param aLocation
     * @return
     */

    public Tile getTileAtLocation(Coordinate aLocation){
        int temp_x =lines[0].length()-1;
        int temp_y =0;
        List myList = new ArrayList();
        myList=tiles;
        Collections.reverse(myList);


        for(int i = 0; i < myList.size(); i++)
        {
            if(temp_x ==aLocation.getX() & temp_y == aLocation.getY()){
                try {
                    return (Tile) myList.get(i);
                } finally {Collections.reverse(myList);

                } }

            else if (myList.get(i)==null){
                temp_x =lines[0].length()-1;
                temp_y++;}

            else{temp_x=temp_x-1;}

        }
        Collections.reverse(myList);
        return null;
    }

    // Here we basically have our getters and setters and to string method.
    public List<List<Tile>> getTiles() {
        return tiles;
    }
    private void setEntrance(Tile entrance) {
        this.entrance = entrance;
    }
    private void setExit(Tile exit) {
        this.exit = exit;
    }

    public String toString() {
        return "Maze{" + getTiles() +
                '}';
    }

    public enum Direction {
        NORTH,
        SOUTH,
        EAST,
        WEST
    }

    public static class Coordinate {
        private int x;
        private int y;

        public Coordinate(int xIn, int yIn) {
            x = xIn;
            y = yIn;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }




        public String toString() {
            return "(" +
                    x +
                    ", " + y +
                    ')';
        }
    }
}
